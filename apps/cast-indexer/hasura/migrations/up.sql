CREATE TABLE SmartContract
(
    Address VARCHAR(64) NOT NULL,
	Name VARCHAR(255),
    Balance DECIMAL(,),
    Abi VARCHAR(),
	PRIMARY KEY(Address)
 
)

CREATE TABLE Transaction
(
    TxHash VARCHAR(64) NOT NULL,
	BlockNumber INT,
	BlockHash VARCHAR(64),
	From VARCHAR(64),
	To VARCHAR(64),
	Value DECIMAL(,),
    Timestamp TIMESTAMP,
    Input VARCHAR(),
	Gas DECIMAL(,),
	GasPrice DECIMAL(,),
	TransactionIndex INT,
	PRIMARY KEY(TxHash)
);


CREATE TABLE Event
(
    TxHash VARCHAR(64) NOT NULL,
	EventName VARCHAR(255),
	AddressSmartContract VARCHAR(64),
	logIndex INT,
	TransactionIndex INT,
	BlockHash VARCHAR(64),
    Timestamp TIMESTAMP,
    args jsonb(),
	BlockNumber INT,
	PRIMARY KEY(TxHash),
	CONSTRAINT fk_address
		FOREIGN KEY(Address)
			REFERENCES contract(Address)
 
);