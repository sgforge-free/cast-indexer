import {
  HasuraInsertEvent,
  TrackedHasuraEventHandler,
} from '@golevelup/nestjs-hasura';
import { Injectable } from '@nestjs/common';
import { CreateSmartContractInput } from './dto/create-smart-contract.input';
import { UpdateSmartContractInput } from './dto/update-smart-contract.input';
import { SmartContract } from './entities/smart-contract.entity';

@Injectable()
export class SmartContractService {
  create(createSmartContractInput: CreateSmartContractInput) {
    return 'This action adds a new smartContract';
  }

  findAll() {
    return `This action returns all smartContract`;
  }

  findOne(Address: string) {
    return `This action returns a #${Address} smartContract`;
  }

  update(id: number, updateSmartContractInput: UpdateSmartContractInput) {
    return `This action updates a #${id} smartContract`;
  }

  remove(id: number) {
    return `This action removes a #${id} smartContract`;
  }

  /*
  @TrackedHasuraEventHandler({
    triggerName: 'smartContract-created',
    tableName: 'SmartContract',
    definition: { type: 'insert' },
  })
  handleUserCreated(evt: HasuraInsertEvent<SmartContract>) {
    console.log('A new smart-contract was created!');
    console.log('Smart Contract info:', evt.event.data.new);
  }*/
}
