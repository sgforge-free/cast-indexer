import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateSmartContractInput {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;
}
