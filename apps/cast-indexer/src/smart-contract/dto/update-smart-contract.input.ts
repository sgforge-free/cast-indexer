import { CreateSmartContractInput } from './create-smart-contract.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateSmartContractInput extends PartialType(
  CreateSmartContractInput,
) {
  @Field(() => Int)
  id: number;
}
