import { ObjectType, Field, Int, Float } from '@nestjs/graphql';

@ObjectType()
export class SmartContract {
  @Field()
  Address: string;
  @Field((type) => Float, { nullable: true })
  Balance: number;
  @Field({ nullable: true })
  Abi: string;
  @Field({ nullable: true })
  Name: string;
  @Field({ nullable: true })
  city: string;
}
