import { Test, TestingModule } from '@nestjs/testing';
import { SmartContractResolver } from './smart-contract.resolver';
import { SmartContractService } from './smart-contract.service';

describe('SmartContractResolver', () => {
  let resolver: SmartContractResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SmartContractResolver, SmartContractService],
    }).compile();

    resolver = module.get<SmartContractResolver>(SmartContractResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
