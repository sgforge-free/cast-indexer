import { Module } from '@nestjs/common';
import { SmartContractService } from './smart-contract.service';
import { SmartContractResolver } from './smart-contract.resolver';

@Module({
  providers: [SmartContractResolver, SmartContractService],
})
export class SmartContractModule {}
