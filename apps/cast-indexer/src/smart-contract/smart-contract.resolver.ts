import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { SmartContractService } from './smart-contract.service';
import { SmartContract } from './entities/smart-contract.entity';
import { CreateSmartContractInput } from './dto/create-smart-contract.input';
import { UpdateSmartContractInput } from './dto/update-smart-contract.input';

@Resolver(() => SmartContract)
export class SmartContractResolver {
  constructor(private readonly smartContractService: SmartContractService) {}

  @Mutation(() => SmartContract)
  createSmartContract(
    @Args('createSmartContractInput')
    createSmartContractInput: CreateSmartContractInput,
  ) {
    return this.smartContractService.create(createSmartContractInput);
  }

  @Query(() => [SmartContract], { name: 'smartContract' })
  findAll() {
    return this.smartContractService.findAll();
  }

  /*
  @Query(() => SmartContract, { name: 'smartContract' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.smartContractService.findOne(id);
  }*/

  @Query(() => SmartContract, { name: 'smartContract' })
  findOne(@Args('Address', { type: () => String }) Address: string): string {
    return this.smartContractService.findOne(Address);
  }

  @Mutation(() => SmartContract)
  updateSmartContract(
    @Args('updateSmartContractInput')
    updateSmartContractInput: UpdateSmartContractInput,
  ) {
    return this.smartContractService.update(
      updateSmartContractInput.id,
      updateSmartContractInput,
    );
  }

  @Mutation(() => SmartContract)
  removeSmartContract(@Args('id', { type: () => Int }) id: number) {
    return this.smartContractService.remove(id);
  }
}
