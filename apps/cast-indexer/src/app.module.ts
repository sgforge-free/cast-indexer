import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HasuraModule } from '@golevelup/nestjs-hasura';
import { GraphQLModule } from '@nestjs/graphql';
import { SmartContractModule } from './smart-contract/smart-contract.module';
import { join } from 'path';
import { ApolloDriver } from '@nestjs/apollo';

@Module({
  imports: [
    HasuraModule.forRoot(HasuraModule, {
      webhookConfig: {
        secretHeader: 'super-secret-header',
        secretFactory: 'this-is-the-secret',
      },
      managedMetaDataConfig: {
        dirPath: join(process.cwd(), 'hasura/metadata'),
        nestEndpointEnvName: 'NEST_EVENT_WEBHOOK_SECRET_HEADER_VALUE', ///hasura/events
        secretHeaderEnvName: 'HASURA_NESTJS_WEBHOOK_SECRET_HEADER_VALUE',
      },
    }),
    GraphQLModule.forRoot({ driver: ApolloDriver, autoSchemaFile: true }),
    SmartContractModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
