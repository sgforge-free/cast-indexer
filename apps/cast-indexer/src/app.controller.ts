import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { URL_PROVIDER } from './Worker/constants';
import Web3 from 'web3';
import { CONTRACT_ABI, CONTRACT_ADDRESS } from './Worker/constants';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getTransactions(): Promise<string> {
    return await this.appService.getTransactions();
  }
  @Get()
  async getEvents(): Promise<string> {
    return await this.appService.getEvents();
  }
}
