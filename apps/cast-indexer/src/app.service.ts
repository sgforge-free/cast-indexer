import { Injectable } from '@nestjs/common';
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
  useQuery,
  gql,
  ApolloQueryResult,
  FetchResult,
} from '@apollo/client';
import fetch from 'cross-fetch';
import { async } from 'rxjs';
import { ObjectType } from '@nestjs/graphql';
import { Console } from 'console';

@Injectable()
export class AppService {
  async queryGraphAllSmartContract(): Promise<ApolloQueryResult<any>> {
    const client = new ApolloClient({
      link: new HttpLink({
        uri: 'http://localhost:8080/v1/graphql',
        fetch,
        headers: { 'x-hasura-admin-secret': 'myadminsecretkey' },
      }),
      cache: new InMemoryCache(),
    });

    try {
      const contracts = await client.query({
        query: gql`
          query {
            SmartContract {
              Address
              Balance
              Name
              Abi
            }
          }
        `,
      }); //.then((result) => console.log(result))
      return contracts;
    } catch (e) {
      console.log(e);
    }
  }

  async queryGraphSmartContract(
    TxHash: string,
  ): Promise<ApolloQueryResult<any>> {
    const client = new ApolloClient({
      link: new HttpLink({
        uri: 'http://localhost:8080/v1/graphql',
        fetch,
        headers: { 'x-hasura-admin-secret': 'myadminsecretkey' },
      }),
      cache: new InMemoryCache(),
    });

    try {
      const contracts = await client.query({
        query: gql`
          query {
            SmartContract(where: {Address: {_eq: "${TxHash}"}}) {
              Address
              Balance
              Name
              Abi
            }
          }
        `,
      }); //.then((result) => console.log(result))
      return contracts;
    } catch (e) {
      console.log(e);
    }
  }

  async queryGraphAllTransaction(): Promise<ApolloQueryResult<any>> {
    const client = new ApolloClient({
      link: new HttpLink({
        uri: 'http://localhost:8080/v1/graphql',
        fetch,
        headers: { 'x-hasura-admin-secret': 'myadminsecretkey' },
      }),
      cache: new InMemoryCache(),
    });

    try {
      const contracts = await client.query({
        query: gql`
          query {
            Transaction {
              TxHash
              From
              To
              BlockHash
              BlockNumber
              Gas
              GasPrice
              Input
              Timestamp
              TransactionIndex
              Value
            }
          }
        `,
      }); //.then((result) => console.log(result))
      return contracts;
    } catch (e) {
      console.log(e);
    }
  }

  async queryGraphTransaction(TxHash: string): Promise<ApolloQueryResult<any>> {
    const client = new ApolloClient({
      link: new HttpLink({
        uri: 'http://localhost:8080/v1/graphql',
        fetch,
        headers: { 'x-hasura-admin-secret': 'myadminsecretkey' },
      }),
      cache: new InMemoryCache(),
    });

    try {
      const contracts = await client.query({
        query: gql`
          query {
            Transaction(
              where: { TxHash: { _eq: "${TxHash}" } }
            ) {
              TxHash
              From
              To
              BlockHash
              BlockNumber
              Gas
              GasPrice
              Input
              Timestamp
              TransactionIndex
              Value
            }
          }
        `,
      });
      return contracts;
    } catch (e) {
      console.log(e);
    }
  }

  async queryGraphAllEvent(): Promise<ApolloQueryResult<any>> {
    const client = new ApolloClient({
      link: new HttpLink({
        uri: 'http://localhost:8080/v1/graphql',
        fetch,
        headers: { 'x-hasura-admin-secret': 'myadminsecretkey' },
      }),
      cache: new InMemoryCache(),
    });

    try {
      const contracts = await client.query({
        query: gql`
          query {
            Event {
              TxHash
              EventName
              AddressSmartContract
              BlockHash
              BlockNumber
              Timestamp
            }
          }
        `,
      }); //.then((result) => console.log(result))
      return contracts;
    } catch (e) {
      console.log(e);
    }
  }

  async queryGraphEvent(TxHash: string): Promise<ApolloQueryResult<any>> {
    const client = new ApolloClient({
      link: new HttpLink({
        uri: 'http://localhost:8080/v1/graphql',
        fetch,
        headers: { 'x-hasura-admin-secret': 'myadminsecretkey' },
      }),
      cache: new InMemoryCache(),
    });
    try {
      const contracts = await client.query({
        query: gql`
          query {
            Event(where: { TxHash: { _eq: "${TxHash}" } }) {
              AddressSmartContract
              TxHash
              EventName
              BlockNumber
              BlockHash
              logIndex
              args
            }
          }
        `,
      }); //.then((result) => console.log(result))
      return contracts;
    } catch (e) {
      console.log(e);
    }
  }

  async insertContract(
    SmartContractHash: string,
  ): Promise<FetchResult<any, Record<string, any>, Record<string, any>>> {
    const ADD_CONTRACT = gql`
      mutation insert_single_article($object: [SmartContract_insert_input!]!) {
        insert_SmartContract(objects: $object) {
          affected_rows
        }
      }
    `;
    const client = new ApolloClient({
      link: new HttpLink({
        uri: 'http://localhost:8080/v1/graphql',
        fetch,
        headers: { 'x-hasura-admin-secret': 'myadminsecretkey' },
      }),
      cache: new InMemoryCache(),
    });
    try {
      const contracts = await client.mutate({
        mutation: ADD_CONTRACT,
        variables: {
          object: { Address: SmartContractHash },
        },
      }); //.then((result) => console.log(result))
      return contracts;
    } catch (e) {
      console.log(e);
    }
  }

  async insertTransactionHasura(
    transaction,
  ): Promise<FetchResult<any, Record<string, any>, Record<string, any>>> {
    const ADD_TRANSACTION = gql`
      mutation insert_single_article($object: [Transaction_insert_input!]!) {
        insert_Transaction(objects: $object) {
          affected_rows
        }
      }
    `;
    const client = new ApolloClient({
      link: new HttpLink({
        uri: 'http://localhost:8080/v1/graphql',
        fetch,
        headers: { 'x-hasura-admin-secret': 'myadminsecretkey' },
      }),
      cache: new InMemoryCache(),
    });
    try {
      const contracts = await client.mutate({
        mutation: ADD_TRANSACTION,
        variables: {
          object: {
            TxHash: transaction.hash,
            BlockNumber: transaction.blockNumber,
            From: transaction.from,
            To: transaction.to,
            // eslint-disable-next-line @typescript-eslint/no-loss-of-precision
            Value: transaction.value,
            //Timestamp: '2022-03-23 13:35:06+00',
          },
        },
      }); //.then((result) => console.log(result))
      return contracts;
    } catch (e) {
      console.log(e);
    }
  }

  async insertEvent(
    ContractEvent,
  ): Promise<FetchResult<any, Record<string, any>, Record<string, any>>> {
    const ADD_EVENT = gql`
      mutation insert_single_event($object: [Event_insert_input!]!) {
        insert_Event(objects: $object) {
          affected_rows
        }
      }
    `;
    const client = new ApolloClient({
      link: new HttpLink({
        uri: 'http://localhost:8080/v1/graphql',
        fetch,
        headers: { 'x-hasura-admin-secret': 'myadminsecretkey' },
      }),
      cache: new InMemoryCache(),
    });
    try {
      const contracts = await client.mutate({
        mutation: ADD_EVENT,
        variables: {
          object: {
            TxHash: ContractEvent.transactionHash,
            AddressSmartContract: ContractEvent.address,
            BlockHash: ContractEvent.blockHash,
            BlockNumber: ContractEvent.blockNumber,
            EventName: ContractEvent.event,
            logIndex: ContractEvent.logIndex,
            args: ContractEvent.raw,
            TransactionIndex: ContractEvent.transactionIndex,
          },
        },
      }); //.then((result) => console.log(result))
      return contracts;
    } catch (e) {
      console.log(e);
    }
  }

  async getTransactions(): Promise<string> {
    const transaction = await this.queryGraphAllTransaction();
    return JSON.stringify(transaction);
  }

  async getEvents(): Promise<string> {
    const evenement = await this.queryGraphAllEvent();
    return JSON.stringify(evenement);
  }
}
