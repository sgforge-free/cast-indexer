import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { CONTRACT_ADDRESS2, URL_PROVIDER } from './Worker/constants';
import { CONTRACT_ABI, CONTRACT_ADDRESS } from './Worker/constants';
import { AppService } from './app.service';
import Web3 from 'web3';
import {
  getContractEvent,
  getContractTransaction,
  getTransaction,
} from './Worker/worker1';
import { stringify } from 'querystring';

const startBlock = 14167146;
const endBlock = 14167147;

async function checkExistenceTransaction(TxHash: string): Promise<boolean> {
  let isExist = false;
  const app = new AppService();
  const transaction = await app.queryGraphTransaction(TxHash);
  if (typeof transaction['data']['Transaction'] !== 'undefined') {
    if (transaction['data']['Transaction'].length == 0) {
      isExist = false;
    } else {
      console.log(transaction['data']['Transaction'].length);
      isExist = true;
    }
  }
  console.log(isExist);
  return isExist;
}

async function checkExistenceEvent(TxHash: string): Promise<boolean> {
  let isExist = false;
  const app = new AppService();
  const transaction = await app.queryGraphEvent(TxHash);
  if (typeof transaction['data']['Event'] !== 'undefined') {
    if (transaction['data']['Event'].length == 0) {
      isExist = false;
    } else {
      console.log(transaction['data']['Event'].length);
      isExist = true;
    }
  }
  console.log(isExist);
  return isExist;
}

async function checkExistenceSmartContract(TxHash: string): Promise<boolean> {
  let isExist = false;
  const app = new AppService();
  const transaction = await app.queryGraphSmartContract(TxHash);
  if (typeof transaction['data']['SmartContract'] !== 'undefined') {
    if (transaction['data']['SmartContract'].length == 0) {
      isExist = false;
    } else {
      console.log(transaction['data']['SmartContract'].length);
      isExist = true;
    }
  }
  console.log(isExist);
  return isExist;
}

async function transactionProcess(TxHash: string) {
  try {
    const isTransactionExist = await checkExistenceTransaction(TxHash);
    const app = new AppService();
    if (isTransactionExist == true) {
      console.log('Transaction requested is in the database');
      const resultTransactionGraphQL = await app.queryGraphTransaction(TxHash);
      console.log(JSON.stringify(resultTransactionGraphQL));
    } else {
      console.log(
        'Transaction requested is not in the database, we will browse in the Ethereum Blockchain',
      );
      const transaction = await getTransaction(TxHash);
      await app.insertTransactionHasura(transaction);
      const resultTransactionGraphQL = await app.queryGraphTransaction(TxHash);
      console.log(JSON.stringify(resultTransactionGraphQL));
    }
  } catch (e) {
    console.error('TTT :' + e);
  }
}

async function eventsProcess(SmartContractHash: string) {
  try {
    const app = new AppService();
    const events = await getContractEvent(
      CONTRACT_ABI,
      SmartContractHash,
      startBlock,
      endBlock,
    );
    const isContractExist: boolean = await checkExistenceSmartContract(
      SmartContractHash,
    );
    if (isContractExist == false) {
      console.log('Smart contract requested is not in database');
      await app.insertContract(SmartContractHash);
      console.log('Smart contract inserted');
    }
    for (let i = 0; i < events.length; i++) {
      console.log(events[i].transactionHash);
      const isEventExist = await checkExistenceEvent(events[i].transactionHash);

      if (isEventExist == true) {
        console.log('Event requested is in the database');
        const resultTransactionGraphQL = await app.queryGraphEvent(
          events[i].transactionHash,
        );
        console.log(JSON.stringify(resultTransactionGraphQL));
      } else {
        console.log(
          'Event requested is not in the database, we will browse in the Ethereum Blockchain',
        );
        await app.insertEvent(events[i]);
        const resultTransactionGraphQL = await app.queryGraphEvent(
          events[i].transactionHash,
        );
        console.log(JSON.stringify(resultTransactionGraphQL));
      }
    }
  } catch (e) {
    console.error('TTT :' + e);
  }
}

async function SmartContractProcess(SmartContractHash: string) {
  try {
    const app = new AppService();
    const events = await getContractEvent(
      CONTRACT_ABI,
      SmartContractHash,
      startBlock,
      endBlock,
    );
    const isContractExist: boolean = await checkExistenceSmartContract(
      SmartContractHash,
    );
    if (isContractExist == false) {
      console.log('Smart contract requested is not in database');
      await app.insertContract(SmartContractHash);
      console.log('Smart contract inserted');
    }
    for (let i = 0; i < events.length; i++) {
      console.log(events[i].transactionHash);
      const isEventExist = await checkExistenceEvent(events[i].transactionHash);

      if (isEventExist == true) {
        console.log('Event requested is in the database');
        const resultTransactionGraphQL = await app.queryGraphEvent(
          events[i].transactionHash,
        );
        console.log(JSON.stringify(resultTransactionGraphQL));
      } else {
        console.log(
          'Event requested is not in the database, we will browse in the Ethereum Blockchain',
        );
        await app.insertEvent(events[i]);
        const resultTransactionGraphQL = await app.queryGraphEvent(
          events[i].transactionHash,
        );
        console.log(JSON.stringify(resultTransactionGraphQL));
      }
    }
  } catch (e) {
    console.error('TTT :' + e);
  }
}

function InsertHashReturnHash(readline): string {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  let answerU = '';

  rl.question('Insert your hash ', function (answer: string) {
    console.log(answer);
    answerU = answer;
  });
  return answerU;
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  /*
  await transactionProcess(
    '0x71bb49fe8fec6aae943f6858a602525fc2948a2682bb3e1a6b521f9de952cd52',
  );*/

  await eventsProcess('0x98E5790f343b83E850C20fb2D100B28dE5e8AEEB');

  await app.listen(4000);
}
bootstrap();
