import { URL_PROVIDER } from './constants';
import Web3 from 'web3';

export async function getContractEvent(
  abi: any,
  address: string,
  startBlock: number,
  endBlock: number,
): Promise<any> {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const { createAlchemyWeb3 } = require('@alch/alchemy-web3');

  // Using HTTPS
  const web3 = createAlchemyWeb3(
    'https://eth-mainnet.alchemyapi.io/v2/jsY0pYjGcrgzLjWxQuo7lGHw2bvQNEfY',
  );

  const contract = new web3.eth.Contract(abi, address);

  try {
    const events = await contract.getPastEvents('allEvents', {
      fromBlock: 0,
      toBlock: 'latest', // You can also specify 'latest'
    });

    const eventList: string[] = [];
    const eventNotListed: string[] = [];
    const eventNumber = [[0], [0], [0], [0], [0]];
    let init = true;
    console.log('\nCONTRACT NUMBER [ ' + address + ' ]\n');
    // Loop Event
    events.forEach((el: { event: string }) => {
      if (init) {
        console.log('\n[EVENT SAMPLE] :');

        console.log(el);
        init = false;
      }

      eventList.push(el.event);
      switch (el.event) {
        case 'Transfer':
          eventNumber[0][0] = eventNumber[0][0] + 1;
          break;
        case 'Deposit':
          eventNumber[1][0] = eventNumber[1][0] + 1;
          break;
        case 'Withdrawal':
          eventNumber[2][0] = eventNumber[2][0] + 1;
          break;
        case 'Approval':
          eventNumber[3][0] = eventNumber[3][0] + 1;
          break;
        default:
          eventNumber[4][0] = eventNumber[4][0] + 1;
          eventNotListed.push(el.event);
      }
    });
    console.log('\n[ALL EVENTS AVAILABLE] :');
    console.log(eventList); // list containing all events on a specific contract
    console.log(
      `\n[EVENT NUMBER] : \n` +
        eventNumber[0][0] +
        ` Transfer\n` +
        eventNumber[1][0] +
        `Deposit\n` +
        eventNumber[2][0] +
        ` Withdrawal\n` +
        eventNumber[4][0] +
        ` Event not listed\n`,
    );

    if (eventNotListed.length) {
      console.log('Events not listed are ');
      console.log(eventNotListed);
    }
    return events;
  } catch (e) {
    console.error('TTT :' + e);
  }
}

export async function getTransaction(TransactionH: string): Promise<any> {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const { createAlchemyWeb3 } = require('@alch/alchemy-web3');

  // Using HTTPS
  const web3 = createAlchemyWeb3(
    'https://eth-mainnet.alchemyapi.io/v2/jsY0pYjGcrgzLjWxQuo7lGHw2bvQNEfY',
  );
  const transaction = await web3.eth.getTransaction(TransactionH);
  console.log(transaction.hash);
  return transaction;
}

export async function getContractTransaction(
  abi: any,
  address: string,
  startBlock: number,
  endBlock: number,
): Promise<any> {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const { createAlchemyWeb3 } = require('@alch/alchemy-web3');

  // Using HTTPS
  const web3 = createAlchemyWeb3(
    'https://eth-mainnet.alchemyapi.io/v2/jsY0pYjGcrgzLjWxQuo7lGHw2bvQNEfY',
  );

  const contractHash = address.toLowerCase();

  const contract = new web3.eth.Contract(abi, address);

  try {
    const block = await web3.eth.getBlock('latest');
    const number = block.number;
    console.log('Searching block ' + number);

    if (block != null && block.transactions != null) {
      for (const txHash of block.transactions) {
        const transaction = await web3.eth.getTransaction(txHash);
        if (address == transaction.to.toLowerCase()) {
          console.log('Transaction found on block: ' + number);
          console.log({
            address: transaction.from,
            value: web3.utils.fromWei(transaction.value, 'ether'),
            timestamp: new Date(),
          });
        }
      }
    }
  } catch (e) {}
}
